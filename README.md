## WPezPlugins: Instagram Feed via Widget

__An unofficial add-on for Smash Balloon's WordPress plugin Instagram Feed. This add-on plugin enables you to place an IG grid of images via widget.__

https://wordpress.org/plugins/instagram-feed/


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### OVERVIEW

Standard WordPress plugin install. 

Look for the Instagram Feed widget. 

The widget settings are fairly obvious. 

_Note: You still need the Smash Balloon plugin. This is simply a way to use a basic widget (instead of a shortcore) to add it to your website._ 


### FAQ

__1 - Why?__

We were using a different plugin that scraped IG pages to get the images but apparently IG has been blocking certain IP from doing that.

__2- I'm not a developer, can we hire you?__
 
 Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 


### HELPFUL LINKS

- https://gitlab.com/WPezPlugins/wpez-widgets-demo

- https://smashballoon.com/


### TODO

- Better explain the widget settings.


### CHANGE LOG

- v0.0.1 - 28 August 2019
  
   Hey! Ho!! Let's go!!! 

