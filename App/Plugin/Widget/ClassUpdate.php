<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 3/23/2019
 * Time: 9:09 AM
 */

namespace WPezInstagramFeedWidget\App\Plugin\Widget;

use WPezInstagramFeedWidget\App\Core\WidgetAbstract\AbstractUpdate;

class ClassUpdate extends AbstractUpdate {

	public function setUpdate() {

		$arr_widget_fields = $this->_container->settings->fields;
		$get_values        = $this->_container->get_values;

		$instance = [];

        $str_key = 'bool_remove_header';
        $arr = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
        if ( $arr['active'] === true ) {
            $instance[ $arr['name'] ] = $get_values->getBool( $arr );
        }

		$str_key = 'title';
		$arr = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$instance[ $arr['name'] ] = $get_values->getValueElseDefault( $arr );
		}

        $str_key = 'bool_remove_footer';
        $arr = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
        if ( $arr['active'] === true ) {
            $instance[ $arr['name'] ] = $get_values->getBool( $arr );
        }

        $str_key = 'footer_link_text';
        $arr = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
        if ( $arr['active'] === true ) {
            $instance[ $arr['name'] ] = $get_values->getValueElseDefault( $arr );
        }

        $str_key = 'footer_link_url';
        $arr = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
        if ( $arr['active'] === true ) {
            $instance[ $arr['name'] ] = $get_values->getValueElseDefault( $arr );
        }

        $str_key = 'footer_link_url_target';
        $arr = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
        if ( $arr['active'] === true ) {
            $instance[ $arr['name'] ] = $get_values->getValueForOptions( $arr );
        }


        // ----
        /*

		$str_key = 'test_chk_bool';
		$arr = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$instance[ $arr['name'] ] = $get_values->getBool( $arr );
		}

		$str_key = 'test_chk_multi';
		$arr = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$instance[ $arr['name'] ] = $get_values->getMultiForOptions( $arr );
		}

		$str_key = 'test_number_1';
		$arr = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$instance[ $arr['name'] ] = $get_values->getNumber( $arr );
		}

		$str_key = 'test_radio';
		$arr = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$instance[ $arr['name'] ] = $get_values->getValueForOptions( $arr );
		}

		$str_key = 'test_select';
		$arr = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$instance[ $arr['name'] ] = $get_values->getValueForOptions( $arr );
		}

		$str_key = 'test_textarea';
		$arr = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$instance[ $arr['name'] ] = $get_values->getValueElseDefault( $arr );
		}
        */

		return $instance;

	}

}