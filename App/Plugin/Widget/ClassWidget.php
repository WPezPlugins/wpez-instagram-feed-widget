<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 3/23/2019
 * Time: 9:09 AM
 */

namespace WPezInstagramFeedWidget\App\Plugin\Widget;

use WPezInstagramFeedWidget\App\Core\WidgetAbstract\AbstractWidget;


class ClassWidget extends AbstractWidget
{

    protected $_arr_inst_defaults;

    public function __construct($container = false)
    {
        parent::__construct($container);

        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults()
    {

        $this->_arr_inst_defaults = [
            'bool_remove_header' => false,
            'title' => '',
            'bool_remove_footer' => false,
            'footer_link_text' => '',
            'footer_link_url' => '',
            'footer_link_url_target' => '_self'
        ];

    }

    public function getView()
    {

        $container = $this->_container;
        $inst = $this->_widget_instance;
        $args = $this->_widget_args;

        if (is_array($inst)) {
            $inst = array_merge($this->_arr_inst_defaults, $inst);
        } else {
            $inst = $this->_arr_inst_defaults;
        }

        if (function_exists('display_instagram')) {

            $atts = [];

            $str_feed = display_instagram($atts, $content = null);

            if ($inst['bool_remove_header'] === true) {

                $str_replace = '';
                if ( is_string($inst['title']) && ! empty($inst['title'])){
                    $str_replace = '<div class="sb_instagram_header_widget">';
                    $str_replace .= '<span class="wpez-igf-widget-title">';
                    $str_replace .= esc_html($inst['title']);
                    $str_replace .= '</span>';
                    $str_replace .= '</div>';
                }

                $str_feed = preg_replace('#<div class="sb_instagram_header"(.*?)</div>#', $str_replace, $str_feed);
            }

            if ($inst['bool_remove_footer'] === true) {

                $str_replace = '';
                if (
                    is_string($inst['footer_link_text']) && ! empty($inst['footer_link_text'])
                    && filter_var($inst['footer_link_url'], FILTER_VALIDATE_URL)
                    && is_string($inst['footer_link_url_target']) && ! empty($inst['footer_link_url_target'])
                ) {
                    $str_replace = '<div class="sbi_load_widget">';
                    $str_replace .= '<span class="wpez-igf-widget-link">';
                    $str_replace .= '<a href="' . esc_url( $inst['footer_link_url']) .'"';
                    $str_replace .= ' target="' . esc_attr($inst['footer_link_url_target']) . '"';
                    $str_replace .= '>';
                    $str_replace .= esc_html($inst['footer_link_text']);
                    $str_replace .= '</a>';
                    $str_replace .= '</span>';
                    $str_replace .= '</div>';
                }


                $str_feed = preg_replace('#<div id="sbi_load"(.*?)</div>#', $str_replace, $str_feed);

            }

            echo $str_feed;


        } else {
            echo '<!-- Oops. Instagram Feed plugin is MIA -->';
        }

    }

}