<?php

namespace WPezInstagramFeedWidget\App\Plugin\Container\Contents;


class ClassFormElements {

	// Note: You can use the All trait, or use them individually and comment out the ones you're going to use for your widget

	// use  \WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements\TraitAllFormElements;


	use  \WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements\TraitCheckBool;
	use  \WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements\TraitCheckMulti;
	use  \WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements\TraitInput;
	use  \WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements\TraitRadio;
	use  \WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements\TraitSelect;
	use  \WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements\TraitTextarea;

}