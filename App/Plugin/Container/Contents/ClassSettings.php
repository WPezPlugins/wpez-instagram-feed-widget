<?php

namespace WPezInstagramFeedWidget\App\Plugin\Container\Contents;


class ClassSettings {


	protected $_arr_fields;
	protected $_arr_widget_args;


	public function __construct() {

		$this->setProperyDefaults();
	}

	public function __get( $str_prop = false ) {

		switch ( $str_prop ) {

			case 'widget_args':
				return $this->_arr_widget_args;

			case 'fields':
			default:
				return $this->_arr_fields;

		}

	}

	protected function setProperyDefaults() {

		$this->_arr_widget_args = [

				//	'base_id' => 'my_base_id',
				'title'       =>  __( 'Instagram Feed', ' wpez-ig-feed' ),
				//'classname' => 'wpez-gfgw-cta',
				'description' =>  __( 'Places the IGFcontent via a widget.', ' wpez-ig-feed' ),
		];

		$this->_arr_fields = [

            'bool_remove_header' => [
                'label' => __( 'Remove IGF Header', ' wpez-ig-feed' ),
                'name'  => 'bool_remove_header',
                'type'  => 'chk_bool'
            ],

			'title' => [
				'label' => __( 'Display Title', ' wpez-ig-feed' ),
				'name'  => 'title',
				// 'class' => 'widefat',
				'type'  => 'text',
				//	'value' => '',
				//	'sanitize' => 'strip_tags',
				//	'default' => ''
			],

            'bool_remove_footer' => [
                'label' => __( 'Remove IGF Footer', ' wpez-ig-feed' ),
                'name'  => 'bool_remove_footer',
                'type'  => 'chk_bool'
            ],

            'footer_link_text' => [
                'label' => __( 'Footer Link Text', ' wpez-ig-feed' ),
                'name'  => 'footer_link_text',
                // 'class' => 'widefat',
                'type'  => 'text',
                //	'value' => '',
                //	'sanitize' => 'strip_tags',
                //	'default' => ''
            ],

            'footer_link_url' => [
                'label' => __( 'Footer Link URL', ' wpez-ig-feed' ),
                'name'  => 'footer_link_url',
                // 'class' => 'widefat',
                'type'  => 'text',
                //	'value' => '',
                //	'sanitize' => 'strip_tags',
                //	'default' => ''
            ],

            'footer_link_url_target' => [
                // note: for radio the label becomes the fieldset legend
                'label'   => __( 'Open Link In', ' wpez-ig-feed' ),
                'name'    => 'footer_link_url_target',
                'type'    => 'radio',
                'br'      => true,
                'options' => [
                    '_self'   => 'Current Window (_self)',
                    '_blank'   => 'New Window (_blank)',
                ],
                'default' => '_self'
            ],

            /*

            'title' => [
                'label' => __( 'Widget Title', ' wpez-ig-feed' ),
                'name'  => 'title',
                // 'class' => 'widefat',
                'type'  => 'text',
                //	'value' => '',
                //	'sanitize' => 'strip_tags',
                //	'default' => ''
            ],

			'test_chk_multi' => [
				'label'   => __( 'Check Multi', ' wpez-ig-feed' ),
				'name'    => 'test_chk_multi',
				'type'    => 'chk_multi',
				'options' => [
					'chk_one'   => 'Chk One',
					'chk_two'   => 'Chk Two',
					'chk_three' => 'Chk Three',
					'chk_four'  => 'Chk Four',
					'chk_five'  => 'Chk Five'
				],
				'default' => 'chk_two'
			],

			'test_number_1' => [
				'label'   => __( 'Number', ' wpez-ig-feed' ),
				'name'    => 'test_number_1',
				'type'    => 'number',
				'class' => 'tiny-text',
				'min'     => '-5',
				'max'     => '5',
				'step'    => '0.5',
				'default' => '1.5',
				'cast'    => 'float'
			],

			'test_radio' => [
				// note: for radio the label becomes the fieldset legend
				'label'   => __( 'Radio', ' wpez-ig-feed' ),
				'name'    => 'test_radio',
				'type'    => 'radio',
				'br'      => false,
				'options' => [
					'one'   => 'One',
					'two'   => 'Two',
					'three' => 'Three'
				],
				'default' => 'two'
			],

			'test_select' => [
				'label'   => __( 'Select', ' wpez-ig-feed' ),
				'name'    => 'test_select',
				'type'    => 'select',
				'options' => [
					'one'   => 'One',
					'two'   => 'Two',
					'three' => 'Three',
					'four'  => 'Four',
					'five'  => 'Five'
				],
				'default' => 'two'
			],

			'test_textarea' => [
				'label' => __( 'Textarea', ' wpez-ig-feed' ),
				'name'  => 'test_textarea',
				// 'class' => 'widefat',
				'type'  => 'textarea',
				//	'value' => '',
				//	'sanitize' => 'strip_tags',
				//	'default' => ''
			],
            */

		];


	}
}