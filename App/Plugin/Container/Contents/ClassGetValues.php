<?php

namespace WPezInstagramFeedWidget\App\Plugin\Container\Contents;


class ClassGetValues {

	// Note: You can use the All trait, or use them individually and comment out the ones you're going to use for your widget

	// use \WPezInstagramFeedWidget\App\Core\Traits\WidgetGetValues\TraitAllGetValues;

	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetGetValues\TraitGetBool;
	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetGetValues\TraitGetMultiForOptions;
	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetGetValues\TraitGetNumber;
	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetGetValues\TraitGetValueElseDefault;
	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetGetValues\TraitGetValueForOptions;

}