<?php

namespace WPezInstagramFeedWidget\App\Core\Traits\WidgetGetValues;

trait TraitAllGetValues {

	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetGetValues\TraitGetBool;
	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetGetValues\TraitGetMultiForOptions;
	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetGetValues\TraitGetNumber;
	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetGetValues\TraitGetValueElseDefault;
	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetGetValues\TraitGetValueForOptions;

}
