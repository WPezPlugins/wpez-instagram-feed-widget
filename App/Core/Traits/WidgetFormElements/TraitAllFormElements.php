<?php

namespace WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements;

trait TraitAllFormElements {

	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements\TraitCheckBool;
	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements\TraitCheckMulti;
	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements\TraitInput;
	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements\TraitRadio;
	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements\TraitSelect;
	use \WPezInstagramFeedWidget\App\Core\Traits\WidgetFormElements\TraitTextarea;

}