<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 3/23/2019
 * Time: 9:09 AM
 */

namespace  WPezInstagramFeedWidget\App\Core\WidgetAbstract;

abstract class AbstractWidget {

	protected $_container;
	protected $_this_widget;
	protected $_widget_args;
	protected $_widget_instance;

	public function __construct( $container = false ) {

		$this->_container = $container;

	}

	public function setArgs( $args ) {

		$this->_widget_args = $args;

	}

	public function setInstance( $instance ) {

		$this->_widget_instance = $instance;

	}

	public function setThisWidget( \WP_Widget $this_widget ) {

		$this->_this_widget = $this_widget;

	}

	abstract public function getView();
}