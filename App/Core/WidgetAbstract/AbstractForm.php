<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 3/23/2019
 * Time: 9:09 AM
 */

namespace WPezInstagramFeedWidget\App\Core\WidgetAbstract;

abstract class AbstractForm {

	protected $_container;
	protected $_this_widget;
	protected $_widget_instance;

	public function __construct( $container = false ) {

		$this->_container = $container;

	}

	protected function getDefaults() {

		return $arr = [
			'active'   => true,
			'inst_new' => $this->_widget_instance,
			'inst_old' => false,
			'this'     => $this->_this_widget,
			'type'     => false
		];

	}

	public function setInstance( $instance ) {

		$this->_widget_instance = $instance;

	}

	public function setThisWidget( \WP_Widget $this_widget ) {

		$this->_this_widget = $this_widget;

	}

	abstract public function getView();

}